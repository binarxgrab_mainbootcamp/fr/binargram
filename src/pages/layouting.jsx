const Layouting = () => {
  return (
    <div className="p-3">
      <div className="text-center">
        <h1 className="text-2xl font-bold">Flex</h1>
      </div>
      {/* flex */}
      <div className="flex  border border-black h-16 justify-start items-center">
        <div className="bg-red-200 text-red-600 text-center w-1/3 h-6">Kiri</div>
        <div className="bg-green-200 text-green-600 text-center w-1/3 h-6">Tengah</div>
        <div className="bg-blue-200 text-blue-600 text-center w-1/3 h-6">Kanan</div>
      </div>
      {/* /flex */}

      <div className="text-center mt-3 mb-2">
        <h1 className="text-2xl font-bold">Flex (Responsive)</h1>
      </div>
      {/* flex */}
      <div className="flex flex-col lg:flex-row border border-black justify-start items-center">
        <div className="bg-red-200 lg:bg-yellow-200 text-red-600 text-center w-1/3 h-full ">Kiri</div>
        <div className="bg-green-200 lg:bg-black text-green-600 text-center w-1/3 h-full ">Tengah</div>
        <div className="bg-blue-200 lg:bg-indigo-200 text-blue-600 text-center w-1/3 h-full ">Kanan</div>
      </div>
      {/* /flex */}
    </div>
  )
}

export default Layouting;