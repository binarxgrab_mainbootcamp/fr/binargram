import { LineChart, Line, XAxis, YAxis, ResponsiveContainer, CartesianGrid, Tooltip, Legend, Bar, BarChart, PieChart, Pie } from 'recharts';

const LineChartContainer = () => {
  const data = [
    { name: 'Bandung', penduduk_pria: 400.55, penduduk_wanita: 10, penduduk_anak: 2400 },
    { name: 'Jakarta', penduduk_pria: 15.5, penduduk_wanita: 2400, penduduk_anak: 2400 },
    { name: 'Jogja', penduduk_pria: 50, penduduk_wanita: 20, penduduk_anak: 2400 },
    { name: 'Surabaya', penduduk_pria: 50, penduduk_wanita: 200, penduduk_anak: 2400 },
  ];

  return (
    <div className="p-3 border w-full">
      <div className="mb-3">
        <h1 className="text-2xl font-bold">Persentase sebaran penduduk (%)</h1>
      </div>
      <div className="shadow-md rounded-xl p-2 border border-indigo-500 h-64 w-full">
        <ResponsiveContainer width="100%" height="100%">
          <LineChart data={data} >
            <Line type="monotone" dataKey="penduduk_pria" stroke="#1a11ca" />
            <Line type="monotone" dataKey="penduduk_wanita" stroke="#ff9885" />
            <Line type="monotone" dataKey="penduduk_anak" stroke="#76be16" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <CartesianGrid strokeDasharray="3 3" />
          </LineChart>
        </ResponsiveContainer>
      </div >
    </div>
  )
};

const BarChartContainer = () => {
  const data = [
    {
      name: 'Page A',
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Page B',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: 'Page C',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: 'Page D',
      uv: 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: 'Page E',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'Page F',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'Page G',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
  ];

  return (
    <div className="p-3 border w-full">
      <div className="mb-3">
        <h1 className="text-2xl font-bold">Bar Chart</h1>
      </div>
      <div className="shadow-md rounded-xl p-2 border border-indigo-500 h-64 w-full">
        <ResponsiveContainer width="100%" height="100%">
          <BarChart
            // width={500}
            // height={300}
            data={data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="pv" fill="#8884d8" />
            <Bar dataKey="uv" fill="#82ca9d" />
          </BarChart>
        </ResponsiveContainer>
      </div >
    </div>
  )
};

const PieChartContainer = ({ data, name, label, formatter }) => {
  const data01 = [
    { name: 'Liverpool', value: 400 },
    { name: 'MU', value: 300 },
    { name: 'Arsenal', value: 300 },
    { name: 'Chelsea', value: 200 },
    { name: 'Man City', value: 278 },
    { name: 'Totenham', value: 189 },
  ];


  const comments = [
    {
      id: "1",
      createdBy: "rafly",
      message: "komunitasnya keren!",
    },
    {
      id: "2",
      createdBy: "deka",
      message: "baru gabung nih, orang - orangnya ramah",
    },
    {
      id: "3",
      createdBy: "gilang",
      message: "ikut gabung ya temen2 !!!",
    }
  ];

  return (
    <div>
      {
        comments.map(({ id, createdBy, message }) => (
          <div key={id}>
            <div>
              {id}
            </div>
            <div>
              {createdBy}
            </div>
            <div>
              {message}
            </div>
          </div>
        ))
      }
    </div>
  )



  // return (
  //   <div className="p-3 border w-full">
  //     <div className="mb-3">
  //       <h1 className="text-2xl font-bold">Pie Chart</h1>
  //     </div>
  //     <div className="shadow-md rounded-xl p-2 border border-indigo-500 h-64 w-full">
  //       <ResponsiveContainer width="100%" height="100%">
  //         <PieChart >
  //           <Pie
  //             dataKey="value"
  //             isAnimationActive={false}
  //             data={data01}
  //             cx="50%"
  //             cy="50%"
  //             outerRadius={60}
  //             fill="#0901ac"
  //             label
  //           />
  //           <Tooltip />
  //           <Legend />
  //         </PieChart>
  //       </ResponsiveContainer>
  //     </div >
  //   </div>
  // )
};

const ChartsPage = () => {
  return (
    <div className="grid lg:grid-cols-3 gap-2 grid-cols-1 p-3">
      <LineChartContainer />
      <BarChartContainer />
      <PieChartContainer />
    </div>
  )
}

export default ChartsPage;