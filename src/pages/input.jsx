import { EyeIcon, EyeOffIcon } from '@heroicons/react/outline';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Input = ({
  type,
  placeholder,
  error,
}) => {
  const [show, setShow] = useState(false);
  return (
    <React.Fragment>
      <div className={`flex justify-between items-center border ${error ? 'border-red-600' : 'border-gray-400'} rounded-xl p-1`}>
        {
          type === "password" ? (
            <>
              <input type={show ? 'text' : 'password'} className="w-full px-2 py-1 ring-0 outline-none" placeholder={placeholder} />
              <button type="button" className="px-2" onClick={() => setShow(!show)} >
                {
                  show ? <EyeIcon className="h-5 w-5" /> : <EyeOffIcon className="h-5 w-5" />
                }
              </button>
            </>
          ) : (
            <>
              <input type={type} className="w-full px-2 py-1 ring-0 outline-none" placeholder={placeholder} />
            </>
          )
        }
      </div>
      {
        error && (<div className="text-red-600 text-xs py-1">{error}</div>)
      }
    </React.Fragment>
  )
}

Input.propTypes = {
  type: PropTypes.string.isRequired,
  error: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
}

const Responsive = () => {

  return (
    <div className="flex justify-center items-center h-screen">
      <div className="h-3 w-1/4">
        {/* input */}
        <Input type error placeholder="Username" />
        <Input type="password" error={'Password is required'} placeholder="Password" />
        {/* /input */}
      </div>
    </div>
  )
};

export default Responsive;