import { useFormik } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
  interests: Yup.array().max(2, 'Pilihan interest maksimal 2'),
});

const ErrorHandling = () => {
  // const { login: { loading, products, errMessage }, getProducts } = useLoginDispatcher();

  const onSubmit = (values) => {
    console.log("values > ", values);
  }

  const { handleSubmit, handleBlur, handleChange, values, errors } = useFormik({
    initialValues: {
      interests: [],
    },
    validationSchema,
    onSubmit
  });


  return (
    <div className="flex flex-col w-screen h-screen justify-center items-center">
      {/* <div>
        <button onClick={() => handleClickHitMeButton()} type="button" className="rounded-xl bg-red-500 text-white py-2 px-6 hover:bg-red-400">
          Hit Me!
        </button>
      </div> */}
      <form onSubmit={handleSubmit}>
        <label className={`cursor-pointer border p-3 ${values.interests.includes('frontend') ? 'bg-blue-400' : 'bg-transparent'}`}>
          <input onChange={handleChange} onBlur={handleBlur} type="checkbox" name="interests" className="hidden" value="frontend" />
          Frontend
        </label>
        <label className={`cursor-pointer border p-3 ${values.interests.includes('ui-ux') ? 'bg-blue-400' : 'bg-transparent'}`}>
          <input onChange={handleChange} onBlur={handleBlur} type="checkbox" name="interests" className="hidden" value="ui-ux" />
          Design UI / UX
        </label>
        <label className={`cursor-pointer border p-3 ${values.interests.includes('backend') ? 'bg-blue-400' : 'bg-transparent'}`}>
          <input onChange={handleChange} onBlur={handleBlur} type="checkbox" name="interests" className="hidden" value="backend" />
          Backend
        </label>
        <div className="mt-5">
          {JSON.stringify(values)}
        </div>
        <div className="mt-5">
          {JSON.stringify(errors)}
        </div>
      </form>


    </div>
  )
};

export default ErrorHandling;