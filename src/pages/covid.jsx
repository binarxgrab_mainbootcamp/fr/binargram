import axios from "axios";
import { useEffect, useState } from "react";
import { Bar, BarChart, CartesianGrid, Legend, Line, LineChart, Pie, PieChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";

const LineChartContainer = ({ data }) => {

  const labelSwitcher = {
    "jumlah_kasus": "Kasus",
    "jumlah_meninggal": "Meninggal",
    "jumlah_sembuh": "Sembuh",
  };

  return (
    <div className="p-3 border w-full">
      <div className="mb-3">
        <h1 className="text-2xl font-bold">Kasus Covid 19 tiap Provinsi</h1>
      </div>
      <div className="shadow-md rounded-xl p-2 border border-indigo-500 h-64 w-full">
        <ResponsiveContainer width="100%" height="100%">
          <LineChart data={data} margin={{
            left: 35,
            right: 35,
          }}  >
            <Line type="monotone" dataKey="jumlah_kasus" name="Kasus" stroke="#8884d8" />
            <Line type="monotone" dataKey="jumlah_meninggal" name="Meninggal" stroke="#d62c0e" />
            <Line type="monotone" dataKey="jumlah_sembuh" name="Sembuh " stroke="#0ed693" />
            <XAxis dataKey="key" />
            <YAxis />
            {/* <Tooltip formatter={(value, name, props) => [value, labelSwitcher[name]]} />
            <Legend formatter={(value, entry, index) => {
              return labelSwitcher[value];
            }} /> */}
            <Tooltip />
            <Legend />
          </LineChart>
        </ResponsiveContainer>
      </div >
    </div>
  )
};

const BarChartContainer = ({ data }) => {
  const labelSwitcher = {
    "penambahan.positif": "Positif",
    "penambahan.sembuh": "Sembuh",
    "penambahan.meninggal": "Meninggal",
  };

  return (
    <div className="p-3 border w-full">
      <div className="mb-3">
        <h1 className="text-2xl font-bold">Penambahan</h1>
      </div>
      <div className="shadow-md rounded-xl p-2 border border-indigo-500 h-64 w-full">
        <ResponsiveContainer width="100%" height="100%">
          <BarChart
            width={500}
            height={300}
            data={data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="key" />
            <YAxis />
            <Tooltip formatter={(value, name, props) => {
              return [value, labelSwitcher[name]]
            }} />
            {/* <Tooltip /> */}
            <Legend formatter={(value, entry, index) => {
              return labelSwitcher[value];
            }} />
            <Bar dataKey="penambahan.positif" fill="#8884d8" />
            <Bar dataKey="penambahan.sembuh" fill="#82ca9d" />
            <Bar dataKey="penambahan.meninggal" fill="#ca8282" />
          </BarChart>
        </ResponsiveContainer>
      </div >
    </div>
  )
};


const CovidPage = () => {

  const [chartData, setChartData] = useState();

  const fetchData = async () => {
    try {
      const response = await axios({
        url: '/covid.json',
        method: 'get',
      });
      console.log("response > ", response.data);
      setChartData(response.data.list_data);
    } catch (error) {
      console.log("error > ", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="grid grid-cols-1 gap-3 lg:grid-cols-2">
      <LineChartContainer data={chartData} />
      <BarChartContainer data={chartData} />
    </div>
  )
};

export default CovidPage;