import { createSlice } from "@reduxjs/toolkit";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { callAPI } from '../../../helpers/network';

const initialState = {
  loading: false,
  products: [],
  errMessage: {
    title: "",
    content: ""
  },
  succesMessage: {}
}

const slices = createSlice({
  initialState,
  name: "login",
  reducers: {
    setErrMessage(state, action) {
      Object.assign(state, {
        ...state,
        errMessage: action.payload,
      })
    },
    setProducts(state, action) {
      Object.assign(state, {
        ...state,
        products: action.payload,
      });
    },
    toggleLoading(state, action) {
      Object.assign(state, {
        ...state,
        loading: action.payload,
      });
    }
  }
});

const { toggleLoading, setProducts, setErrMessage } = slices.actions;

export const useLoginDispatcher = () => {
  const { login } = useSelector((state) => state);
  const dispatch = useDispatch();


  const doLogin = async (values) => {
    dispatch(toggleLoading(true));

    const response = await callAPI({
      url: '/auth/local',
      method: 'POST',
      data: values,
      // headers: {
      //   Authorization: `283263a68150c785b10a05d12981f22c5d4a7bcef768d2df7d77fd87db65ab6861eb54fea4e2238d730e2adc8e3763b62e01c4ac249ea46f4bf918f007f7dcb5785d6b382d7fc81bd1de00c948ad195ae672ede7944912b437a529c0b35f11e3328caafc2edf1ad910e30a635529baa67f1192d4c24fc6e1045a12b40b86d2ed`
      // }
    });

    const { data } = response;

    console.log("data login > ", data);

    localStorage.setItem('jwt', data.jwt);
    localStorage.setItem('user', JSON.stringify(data.user));

    dispatch(toggleLoading(false));
  };

  const getProducts = async () => {
    dispatch(toggleLoading(true));

    try {
      const response = await callAPI({
        url: "/api/products",
        method: "get",
      });
      const { data } = response;
      dispatch(setProducts(data));
      dispatch(toggleLoading(false));

    } catch (error) {
      console.log("error > ", error.response);

      // const errString = error.toString();
      // const errString = error.response.data.error.message;
      const errStatusCode = error.response.status;

      // if (errString.includes('404')) {
      //   dispatch(setErrMessage('tidak ada username dan password'));
      // }

      if (errStatusCode === 404) {
        dispatch(setErrMessage({
          title: "Oops...!",
          content: 'URL API tidak ditemukan'
        }));
      }

      // dispatch(setErrMessage(errString));

      dispatch(toggleLoading(false));
    }



  };

  return {
    login,
    doLogin,
    getProducts,
  }
};

export default slices.reducer;