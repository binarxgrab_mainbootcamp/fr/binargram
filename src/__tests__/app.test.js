import axios from 'axios';
import axiosHttpAdapter from 'axios/lib/adapters/http';
import FormData from 'form-data';

axios.defaults.adapter = axiosHttpAdapter;

describe('App Testing', () => {

  test('penjumlahan 5 + 5 = 10', () => {
    // 3A
    // Arrange
    // Action
    // Assert

    const a = 5; // Arrange
    const b = 5; // Arrange

    const penjumlahan = a + b; // Action
    expect(penjumlahan).toBe(10); // Assert
  });

  test.only("hit api", async () => {

    try {
      const formdata = new FormData();
      formdata.append("postingan", "{\n    \"title\": \"Ini contoh judul text\",\n    \"postText\":\"Halo ini text saya\",\n    \"draft\":false,\n    \"visibility\":false\n}");
      const response = await axios({
        url: "https://markas-gamer.herokuapp.com/mager/postingan",
        method: "POST",
        data: {
          title: "",
          draft: true,
          visibility: "public"
        },
        headers: {
          ...formdata.getHeaders(),
        }
      });
      const { data } = response;
      console.log("data > ", data);
    } catch (error) {
      console.log("error > ", error);
    }
    expect(true).toBe(true);
  });

});

// detail post
const post = {
  title: "",
  content: "",
  visibility: "public",
  draft: false,
  likes: [
    {
      updatedAt: "yyyy-mm-dd",
      createdBy: 1, // -> userId Iman
    },
    {
      updatedAt: "yyyy-mm-dd",
      createdBy: 2, // -> userId Agung
    }
  ]
}

// const userId = getUser(); -> localStorage;
// post.likes.find((item) => item.createdBy === userId); -> state sekarang sudah di like atau unlike

// Like
// 1. kirim postId ke backend
// URL : /mager/post/10/like
// URL : /mager/post/10/unlike
// Token User (JWT) -> user siapa??
// Method : PUT
// -> update data post berdasarkan postId
// -> nambahin siapa yang like ke post tersebut
// --> token user (jwt) -> userId
// --> tanggal update