import { SearchIcon } from "@heroicons/react/outline";
import { SearchInput } from "../../search";

const Navbar = () => {
  return (
    <nav className=" bg-white h-16 fixed top-0 left-0 w-full z-50">
      <div className="max-w-md border-b border-r border-l mx-auto px-3 h-full flex justify-center items-center relative">
        <SearchInput />
      </div>
    </nav>
  )
}

export default Navbar;