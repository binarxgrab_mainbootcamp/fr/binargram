import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import store from '../../../redux/store';
import { Provider } from 'react-redux';
import LoginContainer from '../Login';

import { callAPI } from '../../../helpers/network';

jest.mock('../../../helpers/network');

const MockCallAPI = callAPI;

const RootComponent = ({ children }) => (
  <Provider store={store}>
    {children}
  </Provider>
);

describe('Login Testing', () => {

  beforeEach(() => {
    render(<LoginContainer />, { wrapper: RootComponent });
  });

  test('halaman login sukses di tampilkan dengan memunculkan tulisan Sign In', () => {
    const textSignIn = screen.getByText("Sign In");
    expect(textSignIn).toBeInTheDocument();
  });

  test('melakukan input email pada formulir', () => {
    userEvent.type(screen.getByTestId('input-email'), "agung@binarc.co.id");
    expect(screen.getByTestId('input-email')).toHaveValue("agung@binarc.co.id");
  });

  // jenis test : positive test
  test('melakukan input password pada formulir', () => {
    userEvent.type(screen.getByTestId('input-password'), "12341234");
    expect(screen.getByTestId('input-password')).toHaveValue("12341234");
  });

  // jenis test : negative test
  test("error pada input email", async () => {
    // Arrange
    const inputEmail = screen.getByTestId('input-email');
    // Action
    userEvent.type(inputEmail, "a{backspace}");
    userEvent.tab();
    // Assert
    await waitFor(() => expect(screen.getByTestId("error-email")).toBeInTheDocument());
  });

  test("error pada input password", async () => {
    const inputPassword = screen.getByTestId('input-password');
    userEvent.type(inputPassword, "a{backspace}");
    userEvent.tab();
    await waitFor(() => expect(screen.getByTestId("error-password")).toBeInTheDocument());
  });

  test("submit form login", async () => {

    MockCallAPI.mockResolvedValue({
      data: {
        jwt: "123412341234xxxx",
        user: {
          email: "agung@binarc.co.id"
        }
      }
    });

    const inputEmail = screen.getByTestId('input-email');
    userEvent.type(inputEmail, "agung@binarc.co.id");
    userEvent.tab();

    const inputPassword = screen.getByTestId('input-password');
    userEvent.type(inputPassword, "12341234");
    userEvent.tab();

    const buttonSubmit = screen.getByTestId("button-submit");

    expect(buttonSubmit).toBeInTheDocument();
    expect(inputEmail).toHaveValue("agung@binarc.co.id");
    expect(inputPassword).toHaveValue("12341234");

    userEvent.click(buttonSubmit);

    await waitFor(() => expect(buttonSubmit.innerHTML).toBe("Please wait..."));
  });
});

