import { useState } from "react";

const useRegistration = () => {
  const [loading, setLoading] = useState();
  const submit = (values) => {
    setLoading(true);
    // hit API
    setLoading(false);
  };

  return {
    loading,
    submit,
  }
};

export default useRegistration;